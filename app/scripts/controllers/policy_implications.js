/* Version 1 */

// Taken from geography-controller.js, line 183-400.

        $scope.visualizationPI = function() {
            console.log("Policy Implications Viz");

            var graphPanel = document.getElementById('panel-mi');
            var graphPanelWidth = graphPanel.offsetWidth;
            var graphPanelHeight = graphPanel.offsetHeight;
            // console.log(graphPanelWidth, graphPanelHeight);

            // setup scales for graph layout.
            var graphWidthScale = 0.8;
            var graphHeightScale = 0.7;
            var width = graphPanelWidth * graphWidthScale;
            var height = graphPanelHeight * graphHeightScale;
            // console.log('graph dimensions are: ', width, height);

            // data sources.
            var graph_dataSource = './data/watermark/Ballew_Drains_Original_Modified.csv';
            
            // MODULE private methods.
            function drawGraph() {
                console.log('drawing graph...');

                var paddingTop = 40;
                var paddingBottom = 50;
                var paddingLeft = 100;
                var paddingRight = 100;
                var margin = {top: paddingTop, right: paddingRight, bottom: paddingBottom, left: paddingLeft};

                var x = d3.scale.linear().range([0, width]);
                var y0 = d3.scale.linear().range([height, 0]);
                var y1 = d3.scale.linear().range([height, 0]);

                var numTicks = 8;

                var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom")
                    .ticks(0);

                var yAxisLeft = d3.svg.axis()
                    .scale(y0)
                    .orient("left")
                    .ticks(numTicks);

                var yAxisRight = d3.svg.axis()
                    .scale(y1)
                    .orient("right")
                    .ticks(numTicks); 
                
                // Original line.
                var valueline = d3.svg.line()
                    .x(function(d,i) { 
                        return x(i);
                    })
                    .y(function(d) { 
                        return y1(d.original_drain); 
                    });
                
                // Modified line.
                var valueline2 = d3.svg.line()
                    .x(function(d,i) { 
                        return x(i);
                    })
                    .y(function(d) { 
                        return y1(d.modified_drain); 
                    });

                // Difference line.
                var valueline3 = d3.svg.line()
                    .x(function(d,i) { 
                        return x(i);
                    })
                    .y(function(d) { 
                        return y0(d.difference); 
                    });

                var svg = d3.select("#graph-pi")
                    .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                        .attr("transform", 
                              "translate(" + margin.left + "," + margin.top + ")");

                var tooltip = d3.select("body")
                    .append("div")
                    .style("position", "absolute")
                    .style("z-index", "10")
                    .style("visibility", "hidden")
                    .style("background", "#000")
                    .text("a simple tooltip");

                var mouseMovement = function(d,i) {
                    console.log(d);
                    console.log(i);
                    console.log(d[i]);
                    // console.log(x(d[i]));    // NaN
                };

                // Get the data
                d3.csv(graph_dataSource, function(error, data) {
                   
                    data.forEach(function(d) {
                        d.source = d.dataSource;
                        d.difference = +d.difference;
                        // d.difference = +d.value_O_drains - +d.value_M_drains;  // dynamically calculate difference values.
                        d.original_drain = +d.value_O_drains;
                        d.modified_drain = +d.value_M_drains;
                    });
                    // console.log(data);

                    // Scale the range of the data for the axes.
                    var xMin = 0;
                    var xMax = width;
                    x.domain([xMin, d3.max(data, function(d,i) { 
                        return Math.max(i);
                    })]);

                    var y0Min = -1;
                    var y0Max = 6;
                    y0.domain([y0Min,y0Max]);

                    var y1Min = 0;
                    var y1Max = 25;
                    y1.domain([y1Min,y1Max]);

                    svg.append("path")        // Add the valueline path. original
                        .datum(data)
                        .style("stroke", "#CD6AD4")
                        .style("fill", "none")
                        .attr("class", "original")
                        .attr("d", valueline(data))
                        // .on('mousemove', mouseMovement);
                        .on('mousemove', function(d,i) {
                            mouseMovement(d,i);

                            var dataValueXPosition = d3.mouse(this)[0];
                            var dataValuePositionXScale = x(dataValueXPosition);
                            var dataValuePositionXScaleInvert = x.invert(dataValueXPosition);
                            
                            console.log(dataValueXPosition);
                            console.log(dataValuePositionXScale);
                            console.log(dataValuePositionXScaleInvert);
                        });

                    svg.append("path")        // Add the valueline2 path. modified
                        .datum(data)
                        .style("stroke", "#8AE5F2")
                        .style("fill", "none")
                        .attr("class", "modified")
                        .attr("d", valueline2(data))
                        // .on('mousemove', mouseMovement)
                        .on('mousemove', function(d,i) {
                            mouseMovement(d,i);

                            var dataValueXPosition = d3.mouse(this)[0];
                            var dataValuePositionXScale = x(dataValueXPosition);
                            var dataValuePositionXScaleInvert = x.invert(dataValueXPosition);
                            
                            console.log(dataValueXPosition);
                            console.log(dataValuePositionXScale);
                            console.log(dataValuePositionXScaleInvert);
                        });

                    svg.append("path")        // Add the valueline3 path. difference
                        .datum(data)
                        .style("stroke", "yellow")
                        .style("fill", "none")
                        .attr("class", "difference")
                        .attr("d", valueline3(data))
                        // .on('mousemove', mouseMovement)
                        .on('mousemove', function(d,i) {
                            console.log(this);

                            // mouseMovement(d,i);

                            // var dataValueXPosition = d3.mouse(this)[0];
                            // var dataValuePositionXScale = x(dataValueXPosition);
                            // var dataValuePositionXScaleInvert = x.invert(dataValueXPosition);
                            
                            // console.log(dataValueXPosition);
                            // console.log(dataValuePositionXScale);
                            // console.log(dataValuePositionXScaleInvert);
                        });

                    svg.append("g")            // Add the X Axis
                        .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g")             // Add the Y Axis Left
                        .attr("class", "y axis")
                        .call(yAxisLeft)
                        .append('text')
                        .attr('transform', 'rotate(-90)')
                        .attr('y', -50)
                        .attr('x', -25)
                        .style('text-anchor', 'end')
                        .text('Difference between Original and Modified Scenario Springflow')
                        .attr('class', 'pi-y-axis-label');

                    svg.append("g")             // Add the Y Axis Right      
                        .attr("class", "y axis")
                        .attr("transform", "translate(" + width + " ,0)")   
                        .call(yAxisRight)
                        .append('text')
                        .attr('transform', 'rotate(90)')
                        .attr('y', -50)
                        .attr('x', 225)
                        .style('text-anchor', 'end')
                        .text('Springflow (cfs)')
                        .attr('class', 'pi-y-axis-label');
                });
            };

            drawGraph();
        };