/* Testing Line Graph */

// Taken from geography-controller.js, line 183-323.

        $scope.visualizationPI = function() {
            console.log("Policy Implications Viz");

            var graphPanel = document.getElementById('panel-pi');
            var graphPanelWidth = graphPanel.offsetWidth;
            var graphPanelHeight = graphPanel.offsetHeight;
            // console.log(graphPanelWidth, graphPanelHeight);

            // setup scales for graph layout.
            var graphWidthScale = 0.8;
            var graphHeightScale = 0.7;
            var width = graphPanelWidth * graphWidthScale;
            var height = graphPanelHeight * graphHeightScale;
            // console.log('graph dimensions are: ', width, height);

            // data sources.
            var graph_dataSource = './data/watermark/test_data.tsv';
            
            // MODULE private methods.
            function drawGraph() {
                console.log('drawing graph...');
                
                var paddingTop = 40;
                var paddingBottom = 50;
                var paddingLeft = 100;
                var paddingRight = 100;
                var margin = {top: paddingTop, right: paddingRight, bottom: paddingBottom, left: paddingLeft};

                width = width - margin.left - margin.right;
                height = height - margin.top - margin.bottom;

                var parseDate = d3.time.format("%d-%b-%y").parse,
                    bisectDate = d3.bisector(function(d) {
                        return d.date;
                    }).left,
                    formatValue = d3.format(",.2f"),
                    formatCurrency = function(d) {
                        return "$" + formatValue(d);
                    };

                var x = d3.time.scale()
                    .range([0, width]);

                var y = d3.scale.linear()
                    .range([height, 0]);

                var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom");

                var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left");

                var line = d3.svg.line()
                    .x(function(d) {
                        return x(d.date);
                    })
                    .y(function(d) {
                        return y(d.close);
                    });

                var svg = d3.select(graphPanel).append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                d3.tsv(graph_dataSource, function(error, data) {
                    if (error) throw error;

                    console.log(data);

                    data.forEach(function(d) {
                        d.date = parseDate(d.date);
                        d.close = +d.close;
                    });

                    data.sort(function(a, b) {
                        return a.date - b.date;
                    });

                    x.domain([data[0].date, data[data.length - 1].date]);
                    y.domain(d3.extent(data, function(d) {
                        return d.close;
                    }));

                    svg.append("g")
                        .attr("class", "x pi-axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g")
                        .attr("class", "y pi-axis")
                        .call(yAxis)
                        .append("text")
                        .attr("transform", "rotate(-90)")
                        .attr("y", 6)
                        .attr("dy", ".71em")
                        .style("text-anchor", "end")
                        .text("Price ($)");

                    svg.append("path")
                        .datum(data)
                        .attr("class", "pi-line")
                        .attr("d", line);

                    var focus = svg.append("g")
                        .attr("class", "pi-focus")
                        .style("display", "none");

                    focus.append("circle")
                        .attr("r", 4.5);

                    focus.append("text")
                        .attr("x", 9)
                        .attr("dy", ".35em");

                    svg.append("rect")
                        .attr("class", "pi-overlay")
                        .attr("width", width)
                        .attr("height", height)
                        .on("mouseover", function() { focus.style("display", null); })
                        .on("mouseout", function() { focus.style("display", "none"); })
                        .on("mousemove", mousemove);

                    function mousemove() {
                        var x0 = x.invert(d3.mouse(this)[0]),
                            i = bisectDate(data, x0, 1),
                            d0 = data[i - 1],
                            d1 = data[i],
                            d = x0 - d0.date > d1.date - x0 ? d1 : d0;
                        focus.attr("transform", "translate(" + x(d.date) + "," + y(d.close) + ")");
                        focus.select("text").text(formatCurrency(d.close));
                    }
                });
            };

            drawGraph();
        };