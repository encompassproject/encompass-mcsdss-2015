Comparative data runs.

-- Ballew
dataSource,                             value_O_drains,                        value_M_drains,          Difference,      units_drains 
08f25a734933154e66c99a5af7600850.csv,   18.0467,                               14.719278,               3.327422         cfs            

-- watermark optimized (old)
dataSource,                             value_O,                                value_M,                units,                          value_O_heads,      value_M_heads,      units,                      Zone_1, Zone_2, Zone_3, Zone_4, Zone_5, Zone_6, Zone_7, Zone_8, Zone_9, Zone_10,    Zone_11
08f25a734933154e66c99a5af7600850.csv,   3390000000,                             2170000000,             ft3 (total for entire model),   780.787,            569.01556,          feet (monthly average),     1.9,    1.5,    2,      2,      2,      1.2,    2,      1,      1.1,    1.9,        1.8

-- Kwon (double row header)
*,                                      bsgam_mf96_rchTRBAR_wells_,             *,                                  *,                                      bsgam_mf96_rchTRMAC_wells_,             *,                                      *,                                      bsgam_mf96_rchTROLD_wells_,             *,                                      *,                                      bsgam_mf96_rchTRNAT_wells_,             *,                                      *
hash,                                   OUT storage,                            OUT wells,                          OUT drains,                             OUT storage,                            OUT wells,                              OUT drains,                             OUT storage,                            OUT wells,                              OUT drains,                             OUT storage,                            OUT wells,                              OUT drains
08f25a734933154e66c99a5af7600850,       8253961216,                             2164310016,                         20645801984,                            11543365632,                            2164315648,                             33079812096,                            8875257856,                             2108490624,                             20516646912,                            11500562432,                            2164315648,                             32047542272


-- Kwon (single row header w/units)
hash                                    bsgam_mf96_rchTRBAR_wells_OUT_storage   bsgam_mf96_rchTRBAR_wells_OUT_wells bsgam_mf96_rchTRBAR_wells_OUT_drains    bsgam_mf96_rchTRMAC_wells_OUT_storage   bsgam_mf96_rchTRMAC_wells_OUT_wells     bsgam_mf96_rchTRMAC_wells_OUT_drains    bsgam_mf96_rchTROLD_wells_OUT_storage   bsgam_mf96_rchTROLD_wells_OUT_wells     bsgam_mf96_rchTROLD_wells_OUT_drains    bsgam_mf96_rchTRNAT_wells_OUT_storage   bsgam_mf96_rchTRNAT_wells_OUT_wells     bsgam_mf96_rchTRNAT_wells_OUT_drains    units                                               
08f25a734933154e66c99a5af7600850,       8253961216,                             2164310016,                         20645801984,                            11543365632,                            2164315648,                             33079812096,                            8875257856,                             2108490624,                             20516646912,                            11500562432,                            2164315648,                             32047542272                             ft3

