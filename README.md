# MCSDSS 2015 #

Description pending

### General Information ###

* Project Lifecycle: Start Dev May 21, 2015
* Version: 1.0.0

### Getting Started ###

* Setup
* Configuration
* Dependencies
* Database
* Deployment
* Known Issues

#### Setup ####
1. Clone the repo.
2. npm install
3. bower install
4. grunt serve (for local development)
5. grunt build (for production build)

#### Configuration ####
Pending 

#### Dependencies ####
MCSDSS uses many current libraries and frameworks to enable its functionality. The complete list is located within the package.json file (for the server/development environment dependencies) and the bower.json file (for the client-side dependencies).

#### Database ####
MongoDB currently. PostGIS planned.

#### Deployment ####
Pending

#### Known Issues ####

- Bug with font-awesome glyph icons and bootstrap build script during copy stage.
- TBD

### Points of Contact ###

* Encompass Project - http://www.encompassproject.org
* Dr. Suzanne Pierce (PI) - spierce@tacc.utexas.edu 
* John Gentle (Lead Dev) - jgentle@tacc.utexas.edu

